<?php
/**
 * @file: test_traces_prob.php
 * @info: Tests for the librarie of traces / debug -> DTraz

 * @utor: Moisés Alcocer
 * 2015, <contacto@ironwoods.es>
 * https://www.ironwoods.es
 */

use \DTraz\resources\libs as func;
use \ironwoods\tools\dtraz\DTraz as DTraz;

/**
 * Runs tests
 *
 */

func\ptest('Tests method <b>prob()</b>');
func\ptest('Printing strings...');
DTraz::prob($str);
DTraz::prob($str_xxx);
DTraz::prob($str_vista);
DTraz::prob($str_sql);
/**/
func\ptest('<hr>');
func\ptest('Tests method <b>prob()</b>');
func\ptest('Printing NO visible HTML and colors...');

DTraz::prob($_html);
DTraz::prob($str_sql, 'naranja');
DTraz::prob($_html, 'red');
DTraz::prob($_html, 'azul');
DTraz::prob($_html2, 'azul');
DTraz::prob($_html2, 'blue');

/**/
