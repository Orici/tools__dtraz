<?php
/*
 * @file: colors.php
 * @info: File to test class "Traces"
 *
 * @utor: Moisés Alcocer, 2015
 * <contacto@ironwoods.es>
 * https://www.ironwoods.es
 */


return [
    'azul'          => 'blue',
    'azul claro'    => 'lightblue',
    'azul marino'   => 'navy',
    'azul oscuro'   => 'darkblue',
    'amarillo'      => 'yellow',
    'blanco'        => 'white',
    'dorado'        => 'golden',
    'celeste'       => 'skyblue',
    'gris'          => 'grey',
    'gris claro'    => 'lighgrey',
    'gris oscuro'   => 'darkgrey',
    'lila'          => 'fuchsia',
    'marfil'        => 'ivory',
    'marrón'        => 'brown',
    'marron'        => 'brown',
    'naranja'       => 'orange',
    'negro'         => 'black',
    'rojo'          => 'red',
    'rosa'          => 'pink',
    'oro'           => 'golden',
    'plata'         => 'silver',
    'plateado'      => 'silver',
    'purpura'       => 'fuchsia',
    'púrpura'       => 'fuchsia',
    'verde'         => 'green',
    'verde claro'   => 'lime',
    'verde oliva'   => 'olive',
    'verde oscuro'  => 'olive',
    'violeta'       => 'violet',
];
