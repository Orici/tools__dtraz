<?php
/**
 * @file: requires.php
 * @info: Loads the files
 *
 *
 * @utor: Moisés Alcocer
 * 2017, <contacto@ironwoods.es>
 * https://www.ironwoods.es
 *
 * @package ironwoods.tools.dtraz.libs
 */

$_BASE_PATH = dirname(__FILE__, 3) . "/"; // Only PHP 7


/**
 * Main files
 *
 */
    require_once $_BASE_PATH . "resources/libs/settings.php";


    $ns_class = "\ironwoods\\tools\htmli\\resources\classes\AttrClass";
    if (! class_exists($ns_class)) {
        require_once PATH_VENDORS__DTRAZ . 'ironwoods/htmli/attrclass.class.php';
    }

    require_once BASEPATH_TOOL__DTRAZ . "resources/libs/functions.php";


/**
 * Class files
 *
 */
    require_once PATH_RESOURCES__DTRAZ . "classes/backtrace.php";
    require_once PATH_RESOURCES__DTRAZ . "classes/configfile.php";
    require_once PATH_RESOURCES__DTRAZ . "classes/css.php";
    require_once PATH_RESOURCES__DTRAZ . "classes/debug.php";
    require_once PATH_RESOURCES__DTRAZ . "classes/showclass.php";
    require_once PATH_RESOURCES__DTRAZ . "classes/traces.php";
