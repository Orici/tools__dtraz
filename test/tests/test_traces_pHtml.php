<?php
/**
 * @file: test_traces_pHtml.php
 * @info: Tests for the librarie of traces / debug -> DTraz

 * @utor: Moisés Alcocer
 * 2017, <contacto@ironwoods.es>
 * https://www.ironwoods.es
 */

use \DTraz\resources\libs as func;
use \ironwoods\tools\dtraz\DTraz as DTraz;

/**
 * Runs tests
 *
 */

func\ptest('Tests method <b>pHtml()</b>');
func\ptest('Printing visible HTML...');
DTraz::pHtml($_html);
DTraz::pHtml($_html2);

func\ptest('<hr>');
func\ptest('Tests method <b>pHtml()</b>');
func\ptest('Printing visible HTML with colors...');
DTraz::pHtml($_html, 'blue');
DTraz::pHtml($_html2, 'verde');

/**/
