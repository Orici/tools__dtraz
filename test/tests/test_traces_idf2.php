<?php
/**
 * @file: test_traces_idf2.php
 * @info: Tests for the librarie of traces / debug -> DTraz
 *
 * @utor: Moisés Alcocer
 * 2015, <contacto@ironwoods.es>
 * https://www.ironwoods.es
 */

use \DTraz\resources\libs as func;
use \ironwoods\tools\dtraz\DTraz as DTraz;

/**
 * Runs tests
 *
 */

func\ptest("Printing Charse Set UTF8...");
DTraz::utf8();


func\ptest('<hr>');
func\ptest('Tests method <b>idf / isDefinedFunction()</b><br>');

func\ptest('<hr>');
func\ptest('<span class="coment">Undefined Function</span>');
ironwoods\tools\dtraz\DTraz::idf('prueba()');

func\ptest('<hr>');
func\ptest('<span class="coment">Defined Function</span>');
ironwoods\tools\dtraz\DTraz::idf('rtype()');
/**/
func\ptest('<hr>');
func\ptest('<span class="coment">Undefined Function inside of a class</span>');
$obj->show('pruebas()');

func\ptest('<hr>');
func\ptest('<span class="coment">Defined Function inside of a class</span>');
$obj->show('show()');

func\ptest('<hr>');
func\ptest('<p>Shows extended info...</p>');

func\ptest(
    '<span class="coment">Undefined Function inside of a class</span>'
);
$obj->show('pruebas()', true);

func\ptest('<hr>');
func\ptest(
    '<span class="coment">Defined Function inside of a class</span>'
);
$obj->show('show()', true);


/**/
