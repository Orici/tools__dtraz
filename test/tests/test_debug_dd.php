<?php
/**
 * @file: test_debug_dd.php
 * @info: Tests for the librarie of traces / debug -> DTraz
 *
 * @utor: Moisés Alcocer
 * 2017, <contacto@ironwoods.es>
 * https://www.ironwoods.es
 */


/**
 * Runs tests
 *
 */

/*ironwoods\tools\dtraz\DTraz::dd( 'fin test' );
ironwoods\tools\dtraz\DTraz::dd( $null );
ironwoods\tools\dtraz\DTraz::dd( $false );
ironwoods\tools\dtraz\DTraz::dd( $false, '--- Test message ---' );*/
ironwoods\tools\dtraz\DTraz::dd($arr_fruits, 'Array with fruits...');
/**/
