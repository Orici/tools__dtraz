<?php
/**
 * @file: test_backtrace.php
 * @info: Test class "BackTrace"
 *
 * @utor: Moisés Alcocer
 * 2017, <contacto@ironwoods.es>
 * https://www.ironwoods.es
 */

use \DTraz\resources\clases\BackTrace as BackTrace;
use \DTraz\resources\libs as func;
use \DTraz\test\vars\BackTraceAux as BackTraceAux;

//BackTrace::debug( __FILE__ );
//BackTrace::debug( __FILE__, $msg_test );
func\ptest('<hr>');

//BackTrace::debugFile( __FILE__ );
//BackTrace::debugFile( __FILE__ , $msg_test);
func\ptest('<hr>');

func\ptest(BackTrace::getDebug(__FILE__, true));
func\ptest(BackTrace::getDebug(__FILE__, $msg_test, true));
func\ptest('<hr>');

func\ptest(BackTrace::getFileDebug(__FILE__, true));
func\ptest(BackTrace::getFileDebug(__FILE__, $msg_test, true));
func\ptest('<hr>');
/**/
func\ptest("Guardando trazado en fichero: {$output_file1}");
$res = BackTrace::getFileDebug(__FILE__);
file_put_contents($output_file1, $res);

func\ptest("Guardando trazado en fichero: {$output_file2}");
$res = BackTrace::getFileDebug(__FILE__, $msg_test);
file_put_contents($output_file2, $res);
func\ptest('<hr>');

func\ptest("Guardando trazado en fichero: {$output_file3}");
$res = BackTraceAux::get();
file_put_contents($output_file3, $res);
func\ptest('<hr>');

/**/
