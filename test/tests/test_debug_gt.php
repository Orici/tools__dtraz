<?php
/**
 * @file: test_debug_gt.php
 * @info: Tests for the librarie of traces / debug -> DTraz
 *
 * @utor: Moisés Alcocer
 * 2015, <contacto@ironwoods.es>
 * https://www.ironwoods.es
 */

use \DTraz\resources\clases\Debug as Debug;
use \ironwoods\tools\dtraz\DTraz as DTraz;

/**
 * Runs tests
 *
 */

Debug::gt();
Debug::gt($true);
DTraz::getPointOfCall();
DTraz::getPointOfCall($true);
xxx();

/**/
