<?php namespace DTraz\resources\libs;

/**
 * @file: functions.php
 * @info: File with functions for the librarie
 *
 * @utor: Moisés Alcocer
 * 2015, <contacto@ironwoods.es>
 * https://www.ironwoods.es
 *
 * @package ironwoods.tools.dtraz.libs
 */
//die( 'Loaded "functions.php"...' );

use \ironwoods\tools\htmli\resources\classes\AttrClass as iwAttrClass;

if (! function_exists("addClass")) {

    /**
     * Adds CSS class to a HTML tag
     *
     * @param  string       $html
     * @param  string       $new_class
     * @return string -> HTML
     */
    function addClass($html, $new_class)
    {
        //echo( "Función aux. / addClass() <br>" );

        $ns_class = "ironwoods\\tools\htmli\\resources\\classes\\AttrClass";
        if (class_exists($ns_class)) {
            return iwAttrClass::addClass($html, $new_class);
        }

        echo("Err -> AttrClass don't loaded ! <br>");


        return $html;
    }
}


if (! function_exists("getFileContent")) {

    /**
     * Return text content from a file
     *
     * @param  string       $file_path
     * @return mixed
     */
    function getFileContent($file_path=null)
    {
        //echo( "Función aux. / getFileContent() -> fichero: {$file_path} <br>" ); //traza

        if ($file_path && is_string($file_path)) {

            //El fichero existe
            if (file_exists($file_path)) {
                //leer fichero
                $contenido = file_get_contents($file_path);

                //No pudo abrirse el fichero //false
                if ($contenido !== false) {
                    return $contenido;
                }

                die("Función aux. / getFileContent() -> Fallo lectura del fichero: <b>\"{$file_path}\"</b>");
            } else {
                die("Función aux. / getFileContent() -> Err: no existe el fichero: <b>\"{$file_path}\"</b>");
            } //traza
        } else {
            die("Función aux. / getFileContent() -> Err args");
        } //traza

        return false;
    }
}


if (! function_exists("hasHtml")) {

    /**
     * Comprueba si un string contiene HTML
     *
     * @param  string       $str
     * @return boolean
     */
    function hasHtml($str=null)
    {
        //Compara la cadena con ella misma limpiando los tags html, si los hay son diferentes
        return (strip_tags($str) !== $str);
    }
}


if (! function_exists("ptest")) {

    /**
     * Traces for librarie tests
     *
     * @param  string       $str
     */
    function ptest($str)
    {
        if (defined('DTRAZ_IS_TESTING') && DTRAZ_IS_TESTING) {
            echo('<p style="color:purple">' . $str . '</p>');
        }
    }
}


if (! function_exists("toLower")) {

    /**
     * Convierte a minúsculas una cadena
     * Suple deficiencias de strtolower con caracteres españoles (ñ y acentos)
     *
     * @param  string       $str
     * @return string
     */
    function toLower($str)
    {
        if (! is_string($str)) {
            return false;
        }


        $arr_uppers = array(
            'Á','É','Í','Ó','Ú','Ü','Ç','Ñ','A','B','C','D','E','F','G','H','I',
            'J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'
        );
        $arr_lowers = array(
            'á','é','í','ó','ú','ü','ç','ñ','a','b','c','d','e','f','g','h','i',
            'j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'
        );

        return str_replace($arr_uppers, $arr_lowers, $str);
    }
}
