<?php
/**
 * @file: run_traces_tests.php
 * @info: Tests class "Traces"
 *
 * @utor: Moisés Alcocer
 * 2017, <contacto@ironwoods.es>
 * https://www.ironwoods.es
 */

require "settings.php";
require "vars/settings_traces.php";

use \DTraz\resources\libs as func;
use \ironwoods\tools\dtraz\DTraz as DTraz;

func\ptest('Printing CSS..');
echo '<style>' . DTraz::get_css() . '</style><br>'; //imprime estilos

?>

	<h1>Test librarie "DTraz" / class "Traces"</h1>
	<hr>
	<p>Loading test...</p>

<?php

/*
func\ptest( '<hr><h3>Testing "Debug::idf()"</h3><hr>' );
require "tests/test_traces_idf.php";

func\ptest( '<hr><h3>Testing "Debug::idf()"</h3><hr>' );
require "tests/test_traces_idf2.php";
/**/
func\ptest('<hr><h3>Testing "Traces::pHtml()"</h3><hr>');
require "tests/test_traces_pHtml.php";
/**/
func\ptest('<hr><h3>Testing "Traces::prob()"</h3><hr>');
require "tests/test_traces_prob.php";
/*
func\ptest( '<hr><h3>Testing "Traces::recordTraces()"</h3><hr>' );
require "tests/test_traces_recordTraces.php";

func\ptest( '<hr><h3>Testing "Traces::traceArgs()"</h3><hr>' );
require "tests/test_traces_traceArgs.php";

/**/
