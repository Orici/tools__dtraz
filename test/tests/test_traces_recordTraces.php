<?php
/**
 * @file: test_traces_writeFile.php
 * @info: Tests for the librarie of traces / debug -> DTraz

 * @utor: Moisés Alcocer
 * 2015, <contacto@ironwoods.es>
 * https://www.ironwoods.es
 */

use \DTraz\resources\libs as func;
use \ironwoods\tools\dtraz\DTraz as DTraz;

/**
 * Runs tests
 *
 */

func\ptest('<hr>');
func\ptest('Tests method <b>writeFile()</b>');
func\ptest('Printing a log file...');


$res = (DTraz::writeFile($file_name, $arr_traces))
    ? 'Write log file -> OK'
    : 'Write log file -> ERR';

func\ptest('Test results:');
var_dump($res);

/**/
