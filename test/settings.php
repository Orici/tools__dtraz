<?php
/**
 * @file: settings.php
 * @info: Settings file for testing DTraz classes
 *
 * @utor: Moisés Alcocer
 * 2015, <contacto@ironwoods.es>
 * https://www.ironwoods.es
 */

//Active errors
error_reporting(E_ALL);
ini_set("display_errors", 1);

//Define constant neccesary to test the librarie
define("DTRAZ_IS_TESTING", true);


//Load files
require '../dtraz.php';
require 'vars/myclass.php';
require 'vars/testclass.php';

use \ironwoods\tools\dtraz\DTraz as iwDTraz;

//Prints charset set UTF8
iwDTraz::utf8();
