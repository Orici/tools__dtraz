<?php namespace DTraz\resources\clases;

/**
 * @file: configfile.php
 * @info: Handles config options
 *
 * @utor: Moisés Alcocer
 * 2015, <contacto@ironwoods.es>
 * https://www.ironwoods.es
 *
 * @package ironwoods.tools.dtraz.classes
 */
 
use \DTraz\resources\libs as func;

final class ConfigFile
{

    /**********************************/
    /*** Declaración de Propiedades ***/
        
    private static $clase = 'ConfigFile';
    
    
    /**********************************/
    /*** Declaración de Métodos *******/

    /*** Métodos públicos *************/

    /**
     * Establece colores de trazado mediante pares
     * @param Array $ar_estilos
     * P.e. modelo => azul establece que un string que contenga la palabra modelo se traza en azul
     */
    public static function set_trazado($ar_estilos=null)
    {
        func\ptest(self::$clase . " / set_trazado()");

        if ($ar_estilos && is_array($ar_estilos)) {

                //Guarda array en fichero _estilos.php
            $texto = '<?php return ';
            foreach ($ar_estilos as $contenido) {
                $texto .= $contenido . "\r\n";
            } //Concatenar r\n con comilla simple no funciona
                
            $texto .= ';';

            //Escribe el fichero (borra cualquier contenido anterior)
                //if (file_put_contents(FILE_CSS__DTRAZ, $texto, LOCK_EX))
                //    return TRUE;
        }

        return false;
    }


    /*** Métodos privados *************/
} //class
