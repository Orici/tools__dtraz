<?php namespace DTraz\resources\clases;

/**
 * @file: debug.php
 * @info: Handles debug
 *
 * @utor: Moisés Alcocer
 * 2017, <contacto@ironwoods.es>
 * https://www.ironwoods.es
 *
 * @package ironwoods.tools.dtraz.classes
 */
//die( 'Loading class "Debug"... <br>' );

use \DTraz\resources\libs as func;

final class Debug
{

    /**************************************************************************/
    /*** Properties declaration ***********************************************/
        
    private static $class = 'Debug';
    
    
    /**************************************************************************/
    /*** Methods declaration **************************************************/

    /*** Public Methods ***************/

    /**
     * Debugs content and ends script
     *
     * @param  mixed       $value
     * @param  string      $msg
     */
    public static function dd($value, $msg=null)
    {
        func\ptest(self::$class . " / dd()");

        if ($msg && is_string($msg)) {
            echo '<p class="traza">' . $msg . '</p>';
        }
            
        //Shows trace of the point call
        Debug::gt();


        //Shows var contents and ends the script
        Debug::dx($value, true);
    }

    /**
     * Debugs a var
     *
     * @param  mixed        $value
     * @param  boolean      $finalize -> ends script
     */
    public static function dx($value=null, $finalize=false)
    {
        func\ptest(self::$class . " / dx()");


        if ($value && $value !== true) {
            if (is_array($value)) {
                echo 'Var contents an <b>array</b>';
                self::showFormatted($value);
            } elseif (is_object($value)) {
                echo 'Var contents a <b>object</b>';
                self::showFormatted($value);
            } else {
                if ((int) $value > 0) {
                    echo 'Var contents a <b>number</b>: '
                            . '<b style="color:blue">' . $value . '</b>';
                } elseif (is_string($value)) {
                    echo 'Var contents a <b>string</b>: '
                            . '<b style="color:blue">' . $value . '</b>';
                }
            }
        } else {
            if (is_NULL($value)) {
                echo 'Var contents <b style="color:blue">NULL</b>';
            }

            if ($value === false) {
                echo 'Var contents <b style="color:blue">FALSE</b>';
            }

            if ($value === true) {
                echo 'Var contents <b style="color:blue">TRUE</b>';
            }

            if ($value === 0) {
                echo 'Var contents the number <b style="color:blue">0</b>';
            }

            if (is_array($value)) {
                echo 'Var contents an <b style="color:blue">empty array</b>';
            }
        }
        echo '<br>';


        if ($finalize) {
            die();
        }
    }
        
    /**
     * Shows debug trace of the point call
     *
     * @param  boolean      $extended_info -> gets extended info
     */
    public static function gt($extended_info=false)
    {
        func\ptest(self::$class . " / gt()");

        /**
         * Gets the data
         *
         */

        $dbt = self::getDbt($extended_info);
        //Debug::dx( $dbt, TRUE );
                
        //Sets data
        extract($dbt);

        if (! $extended_info) {
            $object = $arr_args = null;
        }


        /**
         * Prints the data
         *
         */
        func\ptest(self::$class . " / gt() -> Printing data...");

        if ($file) {
            echo("File: <span style='color:blue'>{$file}</span><br>");
        }

        if ($line) {
            echo "Line number <b>{$line}</b> => ";
        }

        if ($type && $class) {
            echo "<b>{$class}</b> / ";
        }

        if ($function) {
            echo "<b style='color:brown'>{$function}()</b>";
        }

        if ($function || ($type && $class)) {
            echo '<br>';
        }

        if ($extended_info) {
            if ($object) {
                Debug::dx($object);
            }

            if ($args) {
                Traces::prob("Argumentos recibidos: ");
                Debug::dx($args);
            }
        }

        echo('<hr>');
    }


    /*** Private Methods **************/


    /**
     * Gets the data from "debug_backtrace()"
     *
     * @param  boolean      $extended_data
     * @return array
     */
    private static function getDbt($extended_data=false)
    {
        func\ptest(self::$class . " / getDbt()");
        //Debug::dx( $pos, TRUE );

        $dbt = debug_backtrace();
        $pos = count($dbt) - 2;
        //Debug::dx( $dbt );
            
        $class = $file = $function = $line = $type = null;
            
        if (isset($dbt[ $pos ][ 'class' ])) {
            $class = $dbt[ $pos ][ 'class' ];
        }

        if (isset($dbt[ $pos ][ 'file' ])) {
            $file = $dbt[ $pos ][ 'file' ];
        }

        if (isset($dbt[ $pos ][ 'line' ])) {
            $line = $dbt[ $pos ][ 'line' ];
        }

        if (isset($dbt[ $pos ][ 'function' ])) {
            $function = $dbt[ $pos ][ 'function' ];
        }

        if (isset($dbt[ $pos ][ 'type' ])) {
            $type = $dbt[ $pos ][ 'type' ];
        }
           
        $arr_data = [
                'file'      => $file,
                'line'      => $line,
                'function'  => $function,
                'class'     => $class,
                'type'      => $type,
            ];
        //Debug::dx( $arr_data, TRUE );

        //No required extended info
        return (! $extended_data)
                ? $arr_data
                : self::getExtended($arr_data, $dbt, $pos);
    }

    /**
     * Aux. getDbt()
     * Gets extended info
     *
     * @param  array        $arr_data
     * @param  array        $dbt
     * @param  int          $pos
     * @return array
     */
    private static function getExtended($arr_data, $dbt, $pos)
    {
        $arr_extended = [

                'object'    => (isset($dbt[ $pos ][ 'object' ]))
                    ? $dbt[ $pos ][ 'object' ]
                    : null,

                'args'      => (isset($dbt[ $pos ][ 'args' ]))
                    ? $dbt[ $pos ][ 'args' ]
                    : null,
            ];
            

        return array_merge($arr_data, $arr_extended);
    }

    /**
     * Shows content for an object / array in several lines
     *
     * @param  mixed        $value
     */
    private static function showFormatted($value)
    {
        func\ptest(self::$class . " / showFormatted()");
            
        echo '<pre style="color:blue">';
        var_dump($value);
        echo '</pre><br>';
    }
} //class
