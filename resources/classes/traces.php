<?php namespace DTraz\resources\clases;

/**
 * @file: traces.php
 * @info: Handle traces / debug
 *
 * @utor: Moisés Alcocer
 * 2015, <contacto@ironwoods.es>
 * https://www.ironwoods.es
 *
 * @package ironwoods.tools.dtraz.classes
 */
//die( 'Loading class Traces... <br>' );

use \DTraz\resources\libs as func;

final class Traces
{

    /**************************************************************************/
    /*** Properties declaration ***********************************************/
        
    private static $class = 'Traces';
    
    
    /**************************************************************************/
    /*** Methods declaration **************************************************/

    /*** Public Methods ***************/



    /**
     * Traces for a file path
     * Composes a message with the filename and directory from a file path
     * and returns it or prints it
     *
     * @param   string      $file_path
     * @param   boolean     $returns
     * @return  string
     */
    public static function filePath($file_path, $returns=false)
    {
        func\ptest(self::$class . " / filePath()");

        $lbreak = ($returns)
                ? PHP_EOL //For printing a file or use with "ddd()" PS function
                : "<br>";

        $str = "Tracing file..." . $lbreak . $lbreak .
                "File --------> " . basename($file_path) . $lbreak .
                "Directory ---> " . dirname($file_path);


        //Returns the trace
        if ($returns) {
            return $str;
        }


        //Prints the trace and stops script execution
        die('<p style="font-family:monospace">' . $str . '</p>');
        exit();
    }

    /**
     * Shows trace and ends the script if the function was declared
     *
     * @param  string       $func_name
     * @param  boolean      $extended_info -> gets extended info
     */
    //public static function isDefFuncion( $func_name ) {
    public static function idf($func_name, $extended_info=false)
    {
        func\ptest(self::$class . " / idf()");

        if (is_string($func_name)) {
            $func_name = str_ireplace('()', '', $func_name);

                
            //Shows trace of the point call
            Debug::gt($extended_info);
                

            $msg = (function_exists($func_name))
                    ? '<span style="color:green">The function <b>'
                        . $func_name . '()</b> is defined.</span><br>'
                    : '<span style="color:red">The function <b>'
                        . $func_name . '()</b> isn\'t defined.</span><br>';

            //Shows message
            echo($msg);
        } else {
            func\ptest("DTraz / idf() -> Err args");
        }
    }

    /**
     * Traces HTML content
     *
     * @param  string       $str
     * @param  string       $color -> Trace color ( default "black")
     */
    public static function pHtml($str, $color=null)
    {

            //Constante no definida o parámetro incorrecto
        if (! is_string($str)) {
            return;
        }

        if (! func\hasHtml($str)) {
            func\ptest(self::$class . " / prob() -> NO HTML");
        }


        $color = ($color)
                ? self::getColor(trim(func\toLower($color)))
                : 'black';  //default color


        //Prints trace
        $css_class = 'c'. $color;
        echo '<div class="traza ' . $css_class . '">'
                . htmlentities($str) . '</div>';
    }
        
    /**
     * Traces a string and break line
     *
     * @param String        $str
     * @param String        $color -> Trace color ( default "black")
     */
    public static function prob($str, $color=null)
    {
        func\ptest(self::$class . " / prob()");

        if (! is_string($str)) {
            return;
        }

        $color = ($color)
                ? self::getColor(trim(func\toLower($color)))
                : 'black';  //default color

                
        //Prints trace
        $css_class = 'c'. $color;
        if (func\hasHtml($str)) {
            echo '<span class="traza '
                    . $css_class . '">'
                    . $str . '</span><br>';
        } else {
            echo '<p class="traza ' . $css_class . '">' . $str . '</p>';
        }
    }
        
    /**
     * Prints charset set UTF8
     *
     */
    public static function utf8()
    {
        //func\ptest( self::$class . " / utf8()" );

        echo '<meta charset="UTF-8">';
    }

    /**
     * Records traces into a log file
     *
     * @param  string       $file_path
     * @param  array        $arr_traces
     * @return boolean
     */
    public static function writeFile($file_path, $arr_traces)
    {
        func\ptest(self::$class . " / writeFile()");

        if (is_string($file_path) && is_array($arr_traces)) {
            $str = "";
            foreach ($arr_traces as $content) {
                $str .= $content . PHP_EOL;
            }

            //Write traces in the file
            return file_put_contents($file_path, $str, FILE_APPEND);
        }


        return false;
    }


    /*** Private Methods **************/

    /**
     * Gets color for the trace
     *
     * @param  string       $color
     * @return string
     */
    private static function getColor($color=null)
    {
        func\ptest(self::$class . " / getColor() -> Color: {$color}");

        if (file_exists(FILE_COLORS__DTRAZ)) {
            $arr_colors = require FILE_COLORS__DTRAZ;
            //Debug::dx( $arr_colors );

            if ($arr_colors && is_array($arr_colors)) {

                    //Valides the color
                foreach ($arr_colors as $color_sp => $color_en) {
                                
                        //Finds the word relationed with color
                    if (($color === $color_sp)
                            || ($color === $color_en)) {
                        return $color_en;
                    }
                }
            } else {
                func\ptest(self::$class
                        . " / getColor() -> Err -> Colors wasn't loaded<br>");
            }
        } else {
            func\ptest(self::$class
                    . " / getColor() -> Err -> Don´t exist: "
                    . FILE_COLORS__DTRAZ . "<br>");
        }

        //func\ptest( 'Traces::getColor() -> Color: ' . $color );
        return false;
    }

    /**
     * Changes HTML tag to avoid interferences with the trace
     * If the HTML code musn´t be show
     *
     * @param  string       $str
     * @return string
     */
    private static function prepareHtml($str)
    {
        func\ptest(self::$class . " / prepareHtml()");

        $open_pos  = strpos($str, '<p');

        if ($open_pos === 0 || $open_pos > 0) {
            $str = str_replace('<p', '<spam', $str);


            $close_pos = strpos($str, '</p>');

            if ($close_pos > 0) {
                $str = str_replace('</p>', '</spam>', $str);
            }
        }


        return $str;
    }

    /**
     * Trace the arguments received by a method / function
     *
     * @param  array        $arr_args
     * @param  string       $method_name
     * @param  string       $class_name    (optional )
     */
    public static function traceArgs(
            $arr_args,
            $method_name=null,
            $class_name=null
        ) {
        echo '<p style="color:green">@@@ Tracing args for <b>';
        if ($class_name && is_string($class_name)) {
            echo ucfirst($class_name);

            if ($method_name && is_string($method_name)) {
                echo " / ";
            }
        }

        if ($method_name && is_string($method_name)) {
            echo $method_name . "() </b></p>";

            if (is_array($arr_args)) {
                foreach ($arr_args as $i => $arg) {
                    echo('<b style="color:blue">Arg number '
                            . ++$i . ': </b>');

                    Debug::dx($arg);
                }

                echo "<hr><br>";
            }
        }
    }
} //class
