<?php
/**
 * @file: run_debug_tests.php
 * @info: Tests class "Debug"
 *
 * @utor: Moisés Alcocer
 * 2017, <contacto@ironwoods.es>
 * https://www.ironwoods.es
 */

require "settings.php";
require "vars/settings_traces.php";

use \DTraz\resources\libs as func;
use \ironwoods\tools\dtraz\DTraz as DTraz;

func\ptest('Printing CSS..');
echo '<style>' . DTraz::get_css() . '</style><br>'; //imprime estilos
                                                    //
?>

	<h1>Test librarie "DTraz" / class "Debug"</h1>
	<hr>
	<p>Loading test...</p>

<?php


func\ptest('<hr><h3>Testing "Debug::dx()"</h3><hr>');
require "tests/test_debug_dx.php";

func\ptest('<hr><h3>Testing "Debug::gt()"</h3><hr>');
require "tests/test_debug_gt.php";

func\ptest('<hr><h3>Testing "Debug::dd()"</h3><hr>');
require "tests/test_debug_dd.php";

/**/
