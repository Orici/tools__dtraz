DTraz - Librer�a para trazado / debug en PHP
Versi�n 0.2.1.1 (15.09.2017)
================================================================================


M�todos disponibles
--------------------------------------------------------------------------------

    dd()                    -> Muestra debug y finaliza script
    debugBackTrace()        -> Imprime un trazado de llamadas (funciones
                                y ficheros)
    debugFileBackTrace()    -> Imprime un trazado de llamadas (s�lo ficheros)
    dx()                    -> Muestra informaci�n sobre lo pasado y su
                                contenido formateado.
    gen_css()               -> Copia la hoja de estilos para las trazas en
                                la ruta dada
    get_css()               -> Devuelve estilos de color para las trazas
    getPointOfCall()        -> Mostrar info del lugar de la llamada
    getDebugBackTrace()     -> Obtiene un trazado de llamadas (funciones
                                y ficheros)
    getFileDebugBackTrace() -> Obtiene un trazado de llamadas (s�lo ficheros)
    idf() / isDefFuncion()  -> Muestra si una funci�n dada esta definida
                                y donde se realiza la comprobaci�n
    pHtml()                 -> Imprime HTML visible en el navegador
                            -> Impresion en negro o color
    prob()                  -> Trazado: imprime un string y un salto de l�nea
                            -> Impresion en negro o color
    traceFilePath()         -> Genera un mensaje de trazado con el path de
                                un fichero
    utf8()		            -> Impresi�n del juego de caracteres
    writeFile()             -> Volcado de un array a fichero de log

    showClassName           -> Muestra el nombre de la clase de un objeto
    showClassProperties     -> Muestra el nombre de las propiedades de la
                                clase de un objeto

Pr�ximos
--------------------------------------------------------------------------------
    set_trazado()           -> Establece configuraci�n del trazado


