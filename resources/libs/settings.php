<?php
/**
 * @file: settings.php
 * @info: Settings file for librarie DTraz
 *
 * @utor: Moisés Alcocer
 * 2017, <contacto@ironwoods.es>
 * https://www.ironwoods.es
 *
 * @package ironwoods.tools.dtraz.libs
 */

//define( "BASEPATH_TOOL__DTRAZ", dirname( __FILE__, 3 ) . "/" ); //Only PHP 7
define("BASEPATH_TOOL__DTRAZ", dirname(dirname(dirname(__FILE__))) . "/");

define("PATH_RESOURCES__DTRAZ", BASEPATH_TOOL__DTRAZ . "resources/");
define("PATH_VENDORS__DTRAZ", PATH_RESOURCES__DTRAZ . "vendors/");

define('FILE_COLORS__DTRAZ', PATH_RESOURCES__DTRAZ . 'data/colors.php');
define('FILE_CSS__DTRAZ', PATH_RESOURCES__DTRAZ . 'css/traces.css');

//define( 'FILE_ERR_LOGS',	BASEPATH_TOOL__DTRAZ . 'logs/err_log.php' );
