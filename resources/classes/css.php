<?php namespace DTraz\resources\clases;

/**
 * @file: css.php
 * @info: Handle CSS estyles
 *
 * @utor: Moisés Alcocer
 * 2015, <contacto@ironwoods.es>
 * https://www.ironwoods.es
 *
 * @package ironwoods.tools.dtraz.classes
 */
//die( 'Loaded class "Css"... <br>' );

use \DTraz\resources\libs as func;

final class Css
{

    /**********************************/
    /*** Declaración de Propiedades ***/
        
    private static $clase = 'Css';
    
    
    /**********************************/
    /*** Declaración de Métodos *******/

    /*** Métodos públicos *************/

    /**
     * Crea una hoja de estilos para las trazas en la ruta dada
     * Realiza la copia de la hoja de estilos incluida en "resources/css/traces.css"
     *
     * @param  String $path_destino
     * @return boolean
     */
    public static function gen_css($path_destino=null)
    {
        func\ptest("DTraz -> " . self::$clase . " / gen_css() -> Devolviendo CSS... <br>");

        //Existe fichero con los estilos
        if (defined('FILE_CSS__DTRAZ') && file_exists(FILE_CSS__DTRAZ)) {
                
                //Pasado string
            if ($path_destino && is_string($path_destino)) {
                    
                    //func\ptest( "DTraz -> " . self::$clase . " / gen_css() -> Copiando fichero CSS...<br>" );
                return copy(FILE_CSS__DTRAZ, $path_destino);

                    
                $mensajeErr = "Path origen: ".FILE_CSS__DTRAZ." @@@ Path destino: {$path_destino}";
                func\ptest("DTraz -> " . self::$clase . " / gen_css() -> Err copiando fichero CSS... <br>{$mensajeErr}");
            }

            return false;

            //Traces
        } else {
            $mensajeErr = "No existe fichero <b>".FILE_CSS__DTRAZ."</b><br>";
            func\ptest($mensajeErr);
        }
            
        func\ptest("DTraz -> " . self::$clase . " / gen_css() -> Err: no existe fichero de estilos");
            
        return false;
    }

    /**
     * Devuelve CSS del fichero de estilos de la librería
     *
     * @param  String $path_estilos
     * @return String
     */
    public static function get_css($path_estilos=null)
    {
        func\ptest("DTraz -> " . self::$clase . " / get_css() -> Devolviendo CSS... <br>");

        //Se pasa path de la hoja de estilos
        if ($path_estilos && is_string($path_estilos)) {
            return func\getFileContent($path_estilos);
        }

        //NO se pasa path de la hoja de estilos
        else {
                
                //Traza
            //func\ptest('NO se pasa path de la hoja de estilos');

            //Se definio path con la hoja de estilos de la librería
            if (defined('FILE_CSS__DTRAZ')) {
                $path_estilos = (defined('PATH_VENDORS'))
                        ? PATH_VENDORS . 'Dtraz/' . FILE_CSS__DTRAZ
                        : FILE_CSS__DTRAZ;
            }

            //Traza
            $mensaje_traza = (file_exists($path_estilos))
                    ? "Existe fichero: <b>\""   .$path_estilos."\"</b><br>"
                    : "No existe fichero: <b>\"".$path_estilos."\"</b><br>";
                
            func\ptest($mensaje_traza);

            //Se tiene un path con la hoja de estilos
            return ($path_estilos)
                    ? func\getFileContent($path_estilos)
                    : false;
        }
    }

    /*** Métodos privados *************/
} //class
