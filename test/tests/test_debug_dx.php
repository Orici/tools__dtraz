<?php
/**
 * @file: test_debug_dx.php
 * @info: Tests for the librarie of traces / debug -> DTraz
 *
 * @utor: Moisés Alcocer
 * 2015, <contacto@ironwoods.es>
 * https://www.ironwoods.es
 */

use \DTraz\resources\clases\Debug as Debug;
use \ironwoods\tools\dtraz\DTraz as DTraz;

/**
 * Runs tests
 *
 */

DTraz::dx($obj);
DTraz::dx($num);
DTraz::dx($false);
DTraz::dx($null);
DTraz::dx($true);
DTraz::dx(0);
DTraz::dx();
DTraz::dx($str);
DTraz::dx($arr_1);
DTraz::dx($arr_2);
DTraz::dx($_html);
DTraz::dx(rtype());
DTraz::dx(xtype());


/**/
