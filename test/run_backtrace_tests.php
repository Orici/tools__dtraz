<?php
/**
 * @file: run_backtrace_tests.php
 * @info: Tests class "BackTrace"
 *
 * @utor: Moisés Alcocer
 * 2017, <contacto@ironwoods.es>
 * https://www.ironwoods.es
 */

require "settings.php";
require "vars/settings_traces.php";
require "vars/backtraceaux.php";

use \DTraz\resources\libs as func;
use \ironwoods\tools\dtraz\DTraz as DTraz;

func\ptest('Printing CSS..');
echo '<style>' . DTraz::get_css() . '</style><br>'; //imprime estilos

?>

	<h1>Test librarie "DTraz" / class "BackTrace"</h1>
	<hr>
	<p>Loading test...</p>

<?php



require "tests/test_backtrace.php";



/**/
