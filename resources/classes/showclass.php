<?php namespace DTraz\resources\clases;

/**
 * @file: showclass.php
 * @info: Handles config options
 *
 * @utor: Moisés Alcocer
 * 2015, <contacto@ironwoods.es>
 * https://www.ironwoods.es
 *
 * @package ironwoods.tools.dtraz.classes
 */

class ShowClass {

    /**
     * Shows the class name
     *
     * @param  mixed    $obj
     */
    public static function name($obj): void
    {
        $reflect   = new \ReflectionClass($obj);
        $className = $reflect->getName();

        echo $className;
    }

    /**
     * Shows the class properties
     *
     * @param  mixed    $obj
     */
    public static function properties($obj): void
    {
        $reflect = new \ReflectionClass($obj);
        $props   = $reflect->getProperties();

        foreach ($props as $prop) {
            $name = $prop->getName();
            echo $name . '<br>';
        }
    }

} // class
