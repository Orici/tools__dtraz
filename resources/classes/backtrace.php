<?php namespace DTraz\resources\clases;

/**
 * @file: backtrace.php
 * @info: Class to handle backtraces
 *
 * @utor: Moisés Alcocer
 * 2017, <contacto@ironwoods.es>
 * https://www.ironwoods.es
 *
 * @package ironwoods.tools.dtraz.classes
 */
//die( 'Loaded class "BackTrace"...' );

use \DTraz\resources\libs as func;

final class BackTrace
{

    /**********************************/
    /*** Properties declaration *******/
        
    private static $clase = 'BackTrace';
    

    /**********************************/
    /*** Methods declaration **********/

    /*** Public Methods ***************/

    /**
     * Prints debug backtrace (functions and files)
     *
     * @param  string 		$file_path
     */
    public static function debug($file_path, $msg=null)
    {
        func\ptest(self::$clase .
                " / debug() -> Backtrace for: <b>" .
                basename($file_path) . "</b>");

        $arr = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);


        $arr_res = array();
        ;
        foreach ($arr as $call) {
            $file = $call[ "file" ];
            $function = $call[ "function" ] . "()";

            $arr_res[] = $function . " / " . $file;
        }

        //Prints a message previously to traces
        if ($msg && is_string($msg)) {
            Traces::prob($msg);
        }

        //Prints traces and ends the script execution
        self::prints($arr_res);
    }

    /**
     * Prints debug backtrace (only files)
     *
     * @param  string 		$file_path
     */
    public static function debugFile($file_path, $msg=null)
    {
        func\ptest(self::$clase .
                " / debugFile() -> Backtrace for: <b>" .
                basename($file_path) . "</b>");

        $arr = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);

    
        //Prints debug backtrace
        $arr_called_files = array();
        foreach ($arr as $call) {
            $arr_called_files[] = $call[ "file" ];
        }
            
        //Prints a message previously to traces
        if ($msg && is_string($msg)) {
            Traces::prob($msg);
        }

        //Prints traces and ends the script execution
        self::prints($arr_called_files);
    }

    /**
     * Gets a debug backtrace (functions and files)
     *
     * @param  string 		$file_path
     * @param  boolean 		$for_html
     * @return string
     */
    public static function getDebug(
            $file_path,
            $msg=null,
            $for_html=false
        ) {
        func\ptest(self::$clase .
                " / getDebug() -> Backtrace for: <b>" .
                basename($file_path) . "</b>");

        $arr = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);


        $str = "";
        if ($msg && is_string($msg)) {
            $str = ($for_html)
                    ? $msg . "<br>"
                    : $msg . PHP_EOL;
        }
        foreach ($arr as $call) {
            $str .= ($for_html)
                    ? $call[ "function" ] . "() / " . $call[ "file" ] . "<br>"
                    : $call[ "function" ] . "() / " . $call[ "file" ] . PHP_EOL;
        }


        return $str;
    }

    /**
     * Gets a string with debug backtrace (only files)
     *
     * @param  string 		$file_path
     * @param  boolean 		$for_html
     * @return string
     */
    public static function getFileDebug(
            $file_path,
            $msg=null,
            $for_html=false
        ) {
        func\ptest(self::$clase .
                " / getFileDebug() -> Backtrace for: <b>" .
                basename($file_path) . "</b>");

        $arr = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);

        $str = "";
        if ($msg && is_string($msg)) {
            $str = ($for_html)
                    ? $msg . "<br>"
                    : $msg . PHP_EOL;
        }

        foreach ($arr as $call) {
            $str .= ($for_html)
                    ? $call[ "file" ] . "<br>"
                    : $call[ "file" ] . PHP_EOL;
        }


        return $str;
    }


    /*** Private Methods **************/

    /**
     * Prints traces and ends the script execution
     *
     * @param  array 		$arr
     */
    private static function prints($arr)
    {
        echo '<pre style="font-family:monospace">';
        print_r($arr);
        echo '</pre>';

        die();
        exit();
    }
} //class
