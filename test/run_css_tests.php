<?php
/**
 * @file: run_css_tests.php
 * @info: Tests class "Css"
 *
 * @utor: Moisés Alcocer
 * 2017, <contacto@ironwoods.es>
 * https://www.ironwoods.es
 */

require "settings.php";
require "vars/settings_traces.php";

use \DTraz\resources\libs as func;

?>

	<h1>Test librarie "DTraz" / class "Css"</h1>
	<hr>
	<p>Loading test...</p>

<?php


func\ptest('<hr><h3>Testing "Css::dx()"</h3><hr>');
require "tests/test_css.php";



/**/
