<?php
/**
 * @file: run_ShowClass_tests.php
 * @info: Tests class "ShowClass"
 *
 * @utor: Moisés Alcocer
 * 2017, <contacto@ironwoods.es>
 * https://www.ironwoods.es
 */

require "settings.php";
require "vars/settings_traces.php";

use \DTraz\resources\libs as func;
use \DTraz\resources\vars\TestClass as MyClass;
use \ironwoods\tools\dtraz\DTraz as DTraz;

func\ptest('Printing CSS..');
echo '<style>' . DTraz::get_css() . '</style><br>'; //imprime estilos
                                                    //
?>

	<h1>Test librarie "DTraz" / class "ShowClass"</h1>
	<hr>
	<p>Loading test...</p>

<?php

$testClass  = new \DTraz\resources\vars\TestClass;
$testClass2 = new MyClass;

func\ptest('<hr><h3>Testing "ShowClass::name()"</h3><hr>');
DTraz::showClassName($testClass);

func\ptest('<hr><h3>Testing "ShowClass::name()"</h3><hr>');
DTraz::showClassName($testClass2);

func\ptest('<hr><h3>Testing "ShowClass::properties()"</h3><hr>');
DTraz::showClassProperties($testClass);


/**/
