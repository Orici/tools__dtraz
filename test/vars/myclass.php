<?php namespace DTraz\resources\vars;

/**
 * @file: myclass.php
 * @info: class for tests in the librarie of traces / debug -> DTraz
 *
 * @utor: Moisés Alcocer
 * 2015, <contacto@ironwoods.es>
 * https://www.ironwoods.es
 */

use \ironwoods\tools\dtraz\DTraz as DTraz;

class MyClass
{
    public function reo($nombre_funcion=null)
    {
        DTraz::idf($nombre_funcion);
    }
}

/**/
