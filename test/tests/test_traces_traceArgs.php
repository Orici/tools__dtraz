<?php
/**
 * @file: test_traces_traceArgs.php
 * @info: File with tests
 *
 * @utor: Moisés Alcocer
 * 2016, <contacto@ironwoods.es>
 * https://www.ironwoods.es
 */

use \DTraz\resources\libs as func;
use \ironwoods\tools\dtraz\DTraz as DTraz;

/**
 * Test
 *
 */

DTraz::traceArgs($arr_args_a, $str_method_name);
DTraz::traceArgs($arr_args_b, $str_method_name);
DTraz::traceArgs($arr_args_b, $str_method_name, $str_class_name);
