<?php namespace DTraz\test\vars;

/**
 * @file: backtraceaux.php
 * @info: Class to test methods of class "BackTrace"
 *
 * @utor: Moisés Alcocer
 * 2017, <contacto@ironwoods.es>
 * https://www.ironwoods.es
 */
//die( 'Loaded class "BackTraceAux"...' );

class BackTraceAux
{
    public static function get()
    {
        return \DTraz\resources\clases\BackTrace::getDebug(__FILE__);
    }
} //class
