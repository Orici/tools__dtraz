<?php namespace ironwoods\tools\htmli\resources\classes;

/**
 * @file: attrclass.class.php
 * @info: Handle attr "class" in HTML elements
 *
 * @utor: Moisés Alcocer
 * 2017, <contacto@ironwoods.com>
 * https://www.ironwoods.es
 *
 * @package       resources.classes
 */
//die( "Loaded attrclass.class.php" );

final class AttrClass
{
    
    /************************************/
    /*** Properties declaration *********/

    private static $class  = "AttrClass";
            

    /************************************/
    /*** Methods declaration  ***********/

    /*** Public Methods *************/

    /**
     * Adds a CSS class to the first HTML element
     *
     * @param  string       $html
     * @param  string       $new_class
     * @return string
     */
    public static function addClass($html, $new_class)
    {
        if (is_string($html) && is_string($new_class)) {

                //Found close for the HTML tag
            if (stripos($html, '>')) {
                    
                    //Gets the first open tag
                $open_tag = AttrClass::getHtmlOpenTag($html);

                //Gets fragment of HTML without open tag
                $tag_len = mb_strlen($open_tag, 'UTF-8');
                $final_html = mb_substr($html, $tag_len);

                //Re-compose open tag with the CSS class
                    $open_tag = (stripos($html, 'class=')) //exist previous class
                        ? AttrClass::addOtherClass($open_tag, $new_class)
                        : AttrClass::addTheClass($open_tag, $new_class);


                return $open_tag .  $final_html;
            }
        } else {
            die(self::class . " / addClass() -> Err args");
        }


        return $html;
    }


    /*** Private Methods ************/

    /**
     * Gets the first HTML open tag
     *
     * Note: gets also text content previous to the tag. This behaviour is
     * valid in this context. Example: 'Hi <b>Jonh</b>.' -> gets: 'Hi <b>'
     *
     * @param  string       $html
     * @return string
     */
    private static function getHtmlOpenTag($html)
    {
        if (! stripos($html, '>')) {
            return false;
        }


        return strstr($html, '>', true) . '>';
    }

    /**
     * Appends another CSS class to the HTML open tag
     *
     * @param  string       $open_tag
     * @param  string       $new_class
     * @return string
     */
    private static function addOtherClass($open_tag, $new_class)
    {
        $class_attr = ' class="'; //8 chars
        if ($firts_part_len = mb_strpos($open_tag, $class_attr)) {
            $firts_part_len += 8;

            //prob( "Pos: " . $firts_part_len . "<br>" );
            //For: '<p class="xxx">' -> Pos: 10
                
            $firts_part = mb_substr($open_tag, 0, $firts_part_len);
            $final_part = mb_substr($open_tag, $firts_part_len);
            $final_part_len = strlen($final_part) - 2;
            //pHtml( "First part: " . $firts_part );
            //pHtml( "Final part: " . $final_part );

            $classes = mb_substr($final_part, 0, $final_part_len);
            //pHtml( "Old CSS classes: " . $classes );

            $new_classes = trim("{$classes} {$new_class}");
            //pHtml( "New CSS classes: " . $new_classes );


            return $firts_part . $new_classes . '">';
        }
            

        return false;
    }

    /**
     * Adds a CSS class to the HTML open tag
     *
     * @param  string       $open_tag
     * @param  string       $new_class
     * @return string
     */
    private static function addTheClass($open_tag, $new_class)
    {
        $new_part = ' class="' . $new_class . '">';

        //str_replace(search, replace, subject)
        return str_replace('>', $new_part, $open_tag);
    }
    

    /**/
} //class
