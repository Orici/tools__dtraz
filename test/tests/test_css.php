<?php
/**
 * @file: test_css.php
 * @info: Tests for the librarie of traces / debug -> DTraz
 *
 * @utor: Moisés Alcocer
 * 2015, <contacto@ironwoods.es>
 * https://www.ironwoods.es
 */

use \DTraz\resources\libs as func;
use \ironwoods\tools\dtraz\DTraz as DTraz;

func\ptest('<b>Test: DTraz::gen_css() </b>');
func\ptest('Copy CSS file with Traces styles in the path: ');
func\ptest('<b>' . $path_css_file . '</b>');

$res = 'CSS file <b>' . $path_css_file . '</b> created: ';
$res .= DTraz::gen_css($path_css_file) ? ' OK' : ' ERR';
func\ptest($res);

func\ptest('<hr>');
func\ptest('<b>Test: DTraz::get_css() </b>');
$css = DTraz::get_css();
func\ptest('CSS: <br>' . $css);

/**/
