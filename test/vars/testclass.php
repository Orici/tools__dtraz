<?php namespace DTraz\resources\vars;

/**
 * @file: testclass.php
 * @info: class for tests in the librarie of traces / debug -> DTraz
 *
 * @utor: Moisés Alcocer
 * 2015, <contacto@ironwoods.es>
 * https://www.ironwoods.es
 */

use \ironwoods\tools\dtraz\DTraz as DTraz;

final class TestClass
{
    private $class = 'TestClass';

    private $a = 1;
    protected $b = 2;
    public $c = 3;
    private $n = null;


    /**
     * Constructor
     *
     * @param mixed         $abc
     */
    public function __construct(mixed $abc=null)
    {
        if ($abc) {
            $n = $abc;
        }
    }

    /**
     * Gets a property value
     *
     * @param  string       $var
     * @return mixed
     */
    public function get(string $var): mixed
    {
        prob($this->class);

        if ($var === 'a') {
            return $this->a;
        }

        if ($var === 'b') {
            return $this->b;
        }

        if ($var === 'c') {
            return $this->c;
        }

        if ($var === 'n') {
            return $this->n;
        }
    }

    /**
     * Prints trace: info about the definition of the function and call point
     *
     * @param  string       $func_name
     * @param  boolean      $extended_info
     */
    public function show(string $func_name, bool $extended_info=false): void
    {
        DTraz::idf($func_name, $extended_info);
    }

} //class
