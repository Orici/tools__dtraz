<?php
/**
 * @file: test_traces_idf.php
 * @info: Tests for the librarie of traces / debug -> DTraz
 *
 * @utor: Moisés Alcocer
 * 2015, <contacto@ironwoods.es>
 * https://www.ironwoods.es
 */

use \DTraz\resources\libs as func;
use \ironwoods\tools\dtraz\DTraz as DTraz;

/**
 * Runs tests
 *
 */

func\ptest("Printing Charse Set UTF8...");
DTraz::utf8();


func\ptest('<hr>');
func\ptest('Tests method <b>idf()</b>');
func\ptest('Calling functions...');

func\ptest('<hr>');
zzz('First test');

func\ptest('<hr>');
$obj->show('zzz');

func\ptest('<hr>');
$obj->show('yyy');

func\ptest('<hr>');
$obj->show('aaa');

/**/
