<?php namespace ironwoods\tools\dtraz;

/**
 * @file: dtraz.php
 * @info: Main file for the librarie of traces / debug -> DTraz
 *
 * @version: 0.2.23
 *
 * @utor: Moisés Alcocer
 * 2017, <contacto@ironwoods.es>
 * https://www.ironwoods.es
 *
 * @package ironwoods.tools.dtraz
 */
//die( 'Loaded "DTraz"... <br>' );

if (PHP_VERSION < 7) {
    die('DTraz -> ERROR: PHP version in lower to the required');
}

//Loads files
require_once "resources/libs/requires.php";

use \DTraz\resources\clases\BackTrace as BackTrace;
use \DTraz\resources\clases\ConfigFile as ConfigFile;
use \DTraz\resources\clases\Css as Css;
use \DTraz\resources\clases\Debug as Debug;
use \DTraz\resources\clases\ShowClass as ShowClass;
use \DTraz\resources\clases\Traces as Traces;

final class DTraz
{
    private static $cont = 0;


    //Debug y fin del scriot
    public static function dd($value=null, $msg=null)
    {

        //Prints charset set one time
        self::printCharsetSet();

        Debug::dd($value, $msg);
    }

    //Prints debug backtrace (functions and files)
    public static function debugBackTrace($file_path, $msg=null)
    {
        BackTrace::debug($file_path, $msg);
    }

    //Prints debug backtrace (only files)
    public static function debugFileBackTrace($file_path, $msg=null)
    {
        BackTrace::debugFile($file_path, $msg);
    }

    //Debug de una variable
    public static function dx($var=null)
    {
        self::printCharsetSet(); //Prints charset set one time

        Debug::dx($var);
    }

    //Genera hoja de estilos CSS
    public static function gen_css($path=null)
    {
        return Css::gen_css($path);
    }

    //Devuelve estilos CSS
    public static function get_css($path=null)
    {
        return Css::get_css($path);
    }

    //Gets a debug backtrace (functions and files)
    public static function getDebugBackTrace(
        $file_path,
        $msg=null,
        $for_html=false
    ) {
        return BackTrace::getDebug($file_path, $msg, $for_html);
    }

    //Gets a string with debug backtrace (only files)
    public static function getFileDebugBackTrace(
        $file_path,
        $msg=null,
        $for_html=false
    ) {
        return BackTrace::getFileDebug($file_path, $msg, $for_html);
    }

    //Shows debug trace of the point call
    public static function getPointOfCall($extended_info=false)
    {
        return Debug::gt($extended_info);
    }

    // Prints the class name
    public static function showClassName($obj): void
    {
        ShowClass::name($obj);
    }

    // Prints the names of the class properties
    public static function showClassProperties($obj): void
    {
        ShowClass::properties($obj);
    }


    //Comprueba si la función se encuentra definida
    public static function idf($nombre_funcion=null, $inf_extendida=false)
    {
        return Traces::idf($nombre_funcion, $inf_extendida);
    }

    /**
     * Traces HTML content
     *
     * @param  string       $str
     * @param  string       $trace_color
     */
    public static function pHtml($str, $trace_color='black')
    {
        self::printCharsetSet(); //Prints charset set one time

        Traces::pHtml($str, $trace_color);
    }

    /**
     * Traces string content
     *
     * @param  string       $str
     * @param  string       $trace_color
     */
    public static function prob($str, $trace_color='black')
    {
        self::printCharsetSet(); //Prints charset set one time

        Traces::prob($str, $trace_color);
    }

    //Establece opciones de configuración
    public static function set_trazado($estilos=null)
    {
        return ConfigFile::set_trazado($estilos);
    }

    //Trazado de los argumentos que recibe un método / función
    public static function traceArgs(
        $arr_args=null,
        $str_method_name=null,
        $str_class_name=null
    ) {
        Traces::traceArgs($arr_args, $str_method_name, $str_class_name);
    }

    //Traces for a file path
    //Composes a message with the filename and directory from a file path
    //and returns it or prints it
    public static function traceFilePath($file_path, $returns=false)
    {
        if ($returns) {
            return Traces::filePath($file_path, true);
        }

        Traces::filePath($file_path);
    }

    //Trazado de un array a fichero de log
    public static function utf8()
    {
        Traces::utf8();
    }

    //Records traces into a log file
    public static function writeFile($file_path, $arr_traces)
    {
        return Traces::writeFile($file_path, $arr_traces);
    }


    private static function printCharsetSet()
    {
        if (self::$cont === 0) {
            Traces::utf8();
            self::$cont++;
        }
    }
} //class
