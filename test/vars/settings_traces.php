<?php
/*
 * @file: settings_traces.php
 * @info: File to test class "Traces"
 *
 * @utor: Moisés Alcocer,
 * 2017, <contacto@ironwoods.es>
 * https://www.ironwoods.es
 */

use \ironwoods\tools\dtraz\DTraz as iwDTraz;

/**
 * Vars definitions
 *
 */

$_html 		= '<p class="xxx">algo de html</p>';
$_html2		= '<h2>Algo de html</h2>';
$false 		= false;
$file_name  = 'res/log.txt';
$msg_test	= "This is a message...";
$null		= null;
$num		= 54;
$obj		= new \DTraz\resources\vars\TestClass();
$obj2		= new \DTraz\resources\vars\MyClass();
$output_file1 = "res/test_backtrace1.txt";
$output_file2 = "res/test_backtrace2.txt";
$output_file3 = "res/test_backtrace3.txt";
$path_css_file = 'res/traza_x.css';
$str		= 'Hola mundo';
$str_class_name  = 'zzz';
$str_method_name = 'xxx';
$str_sql	= 'Hola mundo SQL';
$str_vista  = 'Hola mundo view';
$str_xxx	= 'Hola mundo xxx';
$true		= true;
$arr_1		= array( 1, 2, 'abc' );
$arr_2		= array( $str, $str_xxx, $str_vista );
$arr_traces = $arr_2;
$arr_fruits = [ 'fruits' => [ "grapes", "pears" ]];
$arr_args_a	= [ 'uno', 2, 3, [ 1, 2, 3, 5 ]];
$arr_args_b	= [ null, 0, 1 ];


/**
 * Func definitions
 *
 */

function rtype()
{
}
function xtype()
{
    echo 666 . ' -> ';
}

function xxx()
{
    iwDTraz::getPointOfCall(true);
}

function yyy()
{
    echo 'función yyy()';
}

function zzz($value=null)
{
    echo $value . '<br>';
    iwDTraz::idf('yyy()');
}
